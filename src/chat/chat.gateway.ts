import { ConnectedSocket, MessageBody, OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer } from "@nestjs/websockets";
import { Socket } from "dgram";

@WebSocketGateway()
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
    @WebSocketServer() server;
    
    users: number = 0;

    async handleConnection(){


    }

    async handleDisconnect(){
        this.users--;
        this.server.to('chat', this.users);

    }

    @SubscribeMessage('message')
    handleEvent
    (
        @MessageBody() message: string,
        @ConnectedSocket() client: Socket,  //esto es para emitir a otro server
    ): any|void 
    {
        this.server.emit('message', message);
        console.log('user joined');
        console.log(message)
        this.handleConnection();
        return message;
    }

    @SubscribeMessage('chat')
    async onChat(id: string, data){
        console.log('user joined')
        this.handleConnection();
        if (id == 'chat') {
            this.server.emit('chat', 'Hola');
            console.log('chat!!', data)
        }
        
    }

    //
    @SubscribeMessage('chat2')
    async onChat2(){
        this.server.emit('chat', 'Hola prrs');
    }

    
}
