import { Injectable } from '@nestjs/common';
import { ChatGateway } from './chat.gateway';

@Injectable()
export class ChatService {
    constructor
    (
        private readonly chatGateway: ChatGateway,
        
    ) 
    {}

    async generalChat(id, dto: string): Promise<any>{
        console.log(id, 'this is the param')
        return this.chatGateway.onChat(id, dto)
   }

}
