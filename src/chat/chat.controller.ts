import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ChatService } from './chat.service';

@Controller('chat')
export class ChatController {
    constructor
    (
        private readonly chatService: ChatService,
        
    ) 
    {}

    @Post('send-notification/:id')
    async generalChat
    (
        @Body('message') dto: string,
        @Param('id') id: string
    )
    {
        return this.chatService.generalChat(id, dto);  
    }
}
